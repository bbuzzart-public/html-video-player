var com;
if (!com) com = {};
if (!com.bbuzzart) com.bbuzzart = {};
com.bbuzzart.video = {};

(function (window) {
    
    com.bbuzzart.video = (function() {

        const SERVER_URL = "http://stream.bbuzzart.com/so/chat";

        let sock;
        var timer;
        var timeStarted = -1;
        var timePlayed = 0;    
        var player;
        var playlist = []
        var index = 0;
        var join = {
            liveId: 1,
            type: 'JOIN',
            writer: '사용자키'
        }

		function Player(_player_id) {

            player = document.getElementById(_player_id);

            player.addEventListener("play", videoStartedPlaying);
            player.addEventListener("playing", videoStartedPlaying);
            player.addEventListener('waiting', videoStartedPlaying);
            player.addEventListener('loadstart', videoStartedPlaying);
            player.addEventListener("ended", videoStoppedPlaying);
            player.addEventListener("pause", videoStoppedPlaying);

            player.onended = function(){
                play();
            }
        }

		Player.prototype.connect = function(user) {

            console.log('Server Connecting...');

            join.writer = user // 사용자 키 세팅

            sock = new SockJS(SERVER_URL);
            sock.onopen = function () {

                // 조인 정보 보내기
                sock.send(JSON.stringify(join));
                
                // 메시지 받기
                sock.onmessage = function (e) {
                    var json = JSON.parse(e.data);
                    console.log(json);
                }
                
                // 에러에러
                sock.onerror = function (e) {
                    console.error(e)
                }

            }

        }

        // 플레이 리스트 추가
		Player.prototype.add = function(video) {
            playlist.push(video)
        }

        // 재생
        Player.prototype.play = function() {
            play();
        }

        function play() {

            console.log(index, playlist.length)

            if(index < playlist.length) {
                let video = playlist[index];
                player.title = video.title;
                player.src = video.url;
                player.type = 'video.mp4';
                player.play();

                index++;
                if (index == playlist.length) {
                    index = 0
                }
            }
        }
            
        function videoStartedPlaying(event) {
            timeStarted = new Date().getTime()/1000;
            if(event.type=="playing") {
                timer = setInterval(function(){
                    sock.send(JSON.stringify({liveId: join.liveId, type: 'PLAYING', message: player.title, writer: join.writer}));
                }, 1000)
            }
        }

        function videoStoppedPlaying(event) {
            if(timeStarted>0) {
                var playedFor = new Date().getTime()/1000 - timeStarted;
                timeStarted = -1;
                timePlayed += playedFor;
            }
            clearInterval(timer)
        }

        return { Player : Player } // 즉시실행 함수 반환 값  
    })();

})(window);